public class BasketballPlayer extends Sportsman {
    private Integer height;
    public void dunk(){
        System.out.println(getName() + " has dunked");
    }

    public BasketballPlayer(String name, Integer age, Integer medialCount, Integer height) {
        super(name, age, medialCount);
        this.height = height;
    }
}
