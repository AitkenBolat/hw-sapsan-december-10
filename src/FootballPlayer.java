public class FootballPlayer extends Sportsman {
    private Integer goals;
    private Integer assists;
    private Integer interceptions;

    public void calculateEfficiency(){
        System.out.println(getName() + " efficciency is " + (assists+goals+interceptions)/3);
    }

    public FootballPlayer(String name, Integer age, Integer medialCount, Integer goals, Integer assists, Integer interceptions) {
        super(name, age, medialCount);
        this.goals = goals;
        this.assists = assists;
        this.interceptions = interceptions;
    }

    public Integer getGoals() {
        return goals;
    }

    public void setGoals(Integer goals) {
        this.goals = goals;
    }

    public Integer getAssists() {
        return assists;
    }

    public void setAssists(Integer assists) {
        this.assists = assists;
    }

    public Integer getInterceptions() {
        return interceptions;
    }

    public void setInterceptions(Integer interceptions) {
        this.interceptions = interceptions;
    }
}
